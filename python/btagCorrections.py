import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/BTV/python/btagCorrectionsFiles.yaml' %
                                    os.environ['CMSSW_BASE'])

class btagSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.isMC:
            if self.year < 2022:
                raise ValueError("Btag json SFs currently implemented for Run3 datasets only")

            if not os.getenv("_corr"):
                os.environ["_corr"] = "_corr"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")

                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            if not os.getenv(f"_btagSF_{self.corrKey}"):
                os.environ[f"_btagSF_{self.corrKey}"] = "_btagSF"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_shape_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg[self.corrKey]["fileName"],
                            corrCfg[self.corrKey]["corrName"]+"_shape"))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_wpcut_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg[self.corrKey]["fileName"],
                            corrCfg[self.corrKey]["corrName"]+"_light"))

                # particleNet SF recommendations, extracted from
                # /cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/BTV/2022_Summer22/btagging.json.gz

                # particleNet_shape (v1)
                # particleNet reshaping scale factors for 2022_Summer22. The scale factors have 8 default
                # uncertainty sources (hf,lf,hfstats1/2,lfstats1/2,cferr1/2). All except the cferr1/2
                # uncertainties are to be applied to light and b jets. The cferr1/2 uncertainties are to be
                # applied to c jets. hf/lfstats1/2 uncertainties are to be decorrelated between years, the others
                # correlated. Additional jes-varied scale factors are supplied to be applied for the jes
                # variations. These are to be correlated with the respective jes uncertainty sources (not
                # additional nuisance parameters!).

                # particleNet_light (v1)
                # Working points values of the b-jet discrimination for particleNet in 2022_Summer22. The
                # L/M/T/XT/XXT working points correspond to 10/1/0.1/0.05/0.01% light jet-misidentification rates.
                # For the working point correction multiple different uncertainty schemes are provided. If only
                # one year is analyzed, the 'up' and 'down' systematics can be used. If multiple data taking eras
                # are analyzed, 'up/down_correlated' and 'up/down_uncorrelated' systematics are provided to be
                # used instead of the 'up/down' ones, which are supposed to be correlated/decorrelated between the
                # different data years.

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_shape_sf_%s(
                            std::string syst, Vint flavor, Vfloat eta, Vfloat pt, Vfloat score) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 20. || fabs(eta[i]) >= 2.5 || score[i] < 0) sf.push_back(1.);
                            else if (flavor[i] == 4) { // for c-jets only
                                if (syst.find("cferr") != std::string::npos || syst.find("central") != std::string::npos) {
                                    sf.push_back(corr_shape_%s.eval({syst, flavor[i], fabs(eta[i]), pt[i], score[i]}));
                                }
                                else sf.push_back(1.);
                            }
                            else { // for all others
                                if (syst.find("cferr") != std::string::npos) {
                                    sf.push_back(1.);
                                }
                                else sf.push_back(corr_shape_%s.eval({syst, flavor[i], fabs(eta[i]), pt[i], score[i]}));
                            }
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_wpcut_sf_%s(
                            std::string syst, std::string wp, Vint flavor, Vfloat eta, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 20. || fabs(eta[i]) >= 2.5 || flavor[i] != 0) sf.push_back(1.);
                            else sf.push_back(corr_wpcut_%s.eval({syst, wp, flavor[i], fabs(eta[i]), pt[i]}));
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, self.corrKey, self.corrKey, self.corrKey))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for wp in self.wps:
            if wp == "shape":
                for syst_name, syst in corrCfg["shape_systematics"]:
                    df = df.Define('btagsf_shape%s' % syst_name,
                                    'get_shape_sf_%s("%s", Jet_hadronFlavour, Jet_eta, Jet_pt, Jet_btagPNetB)' %
                                        (self.corrKey, syst))

                    branches.append('btagsf_shape%s' % syst_name)

            else:
                for syst_name, syst in [("", "central"), ("_up", "up"), ("_down", "down")]:
                    df = df.Define('btagsf_wp%s%s' % (wp, syst_name),
                                    'get_wpcut_sf_%s("%s", "%s", Jet_hadronFlavour, Jet_eta, Jet_pt)' %
                                        (self.corrKey, syst, wp))

                    branches.append('btagsf_wp%s%s' % (wp, syst_name))

        return df, branches

def btagSFRDF(**kwargs):
    """
    Module to obtain btagging SFs with their uncertainties.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: btagSFRDF
            path: Corrections.BTV.btagCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
                isUL: self.dataset.has_tag('ul')
                wps: [shape, L, M, T, XT, XXT]
    """

    return lambda: btagSFRDFProducer(**kwargs)
